import { Column, ColumnType, SimpleColumnOption } from "@intutable/database/dist/types"

export const CHANNEL: string = "user-permissions"
export const GLOBAL_PERMISSION = ""

export const PERMISSIONS_TABLE_STRUCTURE: Column[] = [
    {
        name: "roleId",
        type: ColumnType.integer,
    },
    {
        name: "action",
        type: ColumnType.text,
    },
    {
        name: "subject",
        type: ColumnType.text,
    },
    {
        name: "subjectName",
        type: ColumnType.text,
    },
    {
        name: "conditions",
        type: ColumnType.text,
    },
]
